import { FILE_UPLOAD_REQUEST, FILE_UPLOAD_FAILED } from "./fileUploadActionType"
import Axios from "axios"

export const fileUploadRequest = (file) =>{
    return async (dp) =>{
        dp({
            type : FILE_UPLOAD_REQUEST,
            loading : true,
            success : false
        })
        try{
            const result = await Axios.post(file)
            dp({
                type : FILE_UPLOAD_REQUEST,
                loading : false,
                success : true,
                data : result
            })
        }catch(err){
            dp({
                type : FILE_UPLOAD_FAILED,
                loading : false,
                success : false
            })
        }
    }
}