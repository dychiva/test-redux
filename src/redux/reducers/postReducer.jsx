import { GET_POSTS } from "../actions/posts/postActionType"

const defautlState = {
    data :[]
}
export const postReducer = (state  = defautlState,action) =>{
    switch(action.type){
        case GET_POSTS :
            return{
                ...state,
                data : action.data
            }
        default :
            return state;
    }
}