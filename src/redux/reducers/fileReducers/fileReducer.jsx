import { FILE_UPLOAD_REQUEST, FILE_UPLOAD_SUCCESS } from "../../actions/fileUpload/fileUploadActionType"

const defaultState ={
    loading :false,
    success : false,
    data : null
}

export const fileReducer = (state=defaultState, action) =>{
    switch(action.type){
        case  FILE_UPLOAD_REQUEST:
            return{
                ...state,
                success : action.success,
                loading : action.loading
            };
        case FILE_UPLOAD_SUCCESS :
            return{
                ...state,

            };
        default :
            return state
    }
}