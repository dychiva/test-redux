import React from 'react';
import { connect } from 'react-redux';

const ShowData = (props) => (
  <div className="ShowDataWrapper">
    {
      props.data.map((data,index)=>{
        return <p key={index}>{data.title}</p>
      })
    }
  </div>
);

const mapStateToProps = state => {
  return{
    data : state.postReducer.data
  }
}

const mapDispatchToProps = dispatch => ({
});

export default connect(
  mapStateToProps,
  null,
)(ShowData);
