import React from 'react'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'
import {getPosts, postPosts} from '../../redux/actions/posts/postAction'
const ListPost = (props) =>{
  props.getPosts()
  return(
    <div>
      <button onClick={
        () =>{
          props.getPosts()
        }
      }>Get Posts</button>
      
      {/* {
      props.data.map((data,index)=>{
        return <p key={index}>{data.title}</p>
      })
    }
     <h1>List Post</h1> */}
    </div>
  )
}

const mapStateToProps = (state) =>{
  // return{
  //   data : state.postReducer.data
  // }
}

const mapDispatchToProps = (dispatch)=>{
  const boundActionCreators = bindActionCreators({
    getPosts,
    postPosts
  },dispatch)
  return boundActionCreators
}

export default connect(mapStateToProps, mapDispatchToProps)(ListPost)